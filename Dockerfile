# New Computer Modern fonts (see https://ctan.org/pkg/newcomputermodern)
FROM --platform=$BUILDPLATFORM alpine:latest AS newcm
RUN set -eux; \
    wget "https://download.gnu.org.ua/release/newcm/newcm-5.1.txz"; \
    echo "2c01efed49a04a6f3a5c80fcea27ee32f76c768e36a044c54b00b05a46c06f55 newcm-5.1.txz" | sha256sum -c -; \
    tar xvf newcm-5.1.txz newcm-5.1/otf; \
    mkdir -p /usr/local/share/fonts/otf/NewCM; \
    cp newcm-5.1/otf/*.otf /usr/local/share/fonts/otf/NewCM;

# Inria Fonts (see https://black-foundry.com/case-studies/inria-identity-font/)
FROM --platform=$BUILDPLATFORM alpine:latest AS inria
RUN set -eux; \
    wget -O InriaFonts-1.200.tar.gz "https://github.com/BlackFoundryCom/InriaFonts/archive/refs/tags/v1.200.tar.gz"; \
    echo "2f886aad28c2ceb976b63829e16db888c3f643bbb914afef5829bc39a0e3b004 InriaFonts-1.200.tar.gz" | sha256sum -c -; \
    tar xvf InriaFonts-1.200.tar.gz InriaFonts-1.200/fonts; \
    mkdir -p /usr/local/share/fonts/otf/InriaFonts; \
    cp InriaFonts-1.200/fonts/InriaSans/OTF/*.otf /usr/local/share/fonts/otf/InriaFonts; \
    cp InriaFonts-1.200/fonts/InriaSerif/OTF/*.otf /usr/local/share/fonts/otf/InriaFonts;

# FiraGO fonts (see https://bboxtype.com/typefaces/FiraGO)
FROM --platform=$BUILDPLATFORM alpine:latest AS firago
RUN set -eux; \
    wget -O FiraGO-9882ba0851f88ab904dc237f250db1d45641f45d.tar.gz "https://github.com/bBoxType/FiraGO/archive/9882ba0851f88ab904dc237f250db1d45641f45d.tar.gz"; \
    echo "61ffbbd77665dba15616471fdc6fae8a7151c9f9e400e870bc6bde0b374d8815  FiraGO-9882ba0851f88ab904dc237f250db1d45641f45d.tar.gz" | sha256sum -c -; \
    tar xvf FiraGO-9882ba0851f88ab904dc237f250db1d45641f45d.tar.gz FiraGO-9882ba0851f88ab904dc237f250db1d45641f45d/Fonts/FiraGO_OTF_1001 ; \
    mkdir -p /usr/local/share/fonts/otf/FiraGO; \
    cp FiraGO-9882ba0851f88ab904dc237f250db1d45641f45d/Fonts/FiraGO_OTF_1001/Italic/*.otf /usr/local/share/fonts/otf/FiraGO; \
    cp FiraGO-9882ba0851f88ab904dc237f250db1d45641f45d/Fonts/FiraGO_OTF_1001/Roman/*.otf /usr/local/share/fonts/otf/FiraGO;

# Fira Math font (see https://firamath.github.io/)
FROM --platform=$BUILDPLATFORM alpine:latest AS firamath
RUN set -eux; \
    wget "https://github.com/firamath/firamath/releases/download/v0.3.4/FiraMath-Regular.otf"; \
    echo "2028cbd3dd4d8c0cf1608520eb4759956a83a67931d7b6d8e7c313520186e35b FiraMath-Regular.otf" | sha256sum -c -; \
    mkdir -p /usr/local/share/fonts/otf/FiraMath; \
    mv FiraMath-Regular.otf /usr/local/share/fonts/otf/FiraMath;

# Microsoft fonts from Windows 11
FROM --platform=$BUILDPLATFORM alpine:latest AS ttf-ms-win11
RUN set -eux; \
    apk add --no-cache 7zip; \
    mkdir /ttf-ms-win11; \
    cd /ttf-ms-win11; \
    wget "https://software-static.download.prss.microsoft.com/dbazure/888969d5-f34g-4e03-ac9d-1f9786c66749/26100.1742.240906-0331.ge_release_svc_refresh_CLIENTENTERPRISEEVAL_OEMRET_x64FRE_en-us.iso"; \
    7z e 26100.1742.240906-0331.ge_release_svc_refresh_CLIENTENTERPRISEEVAL_OEMRET_x64FRE_en-us.iso sources/install.wim; \
    7z e install.wim Windows/Fonts/"*".ttf Windows/Fonts/"*".ttc Windows/System32/Licenses/neutral/"*"/"*"/license.rtf -ofonts/; \
    mkdir -p /usr/local/share/fonts/ttf; \
    mv fonts /usr/local/share/fonts/ttf/ttf-ms-win11; \
    rm -r /ttf-ms-win11;

FROM ghcr.io/typst/typst:v0.12.0 AS typst
ENV TYPST_FONT_PATHS=/usr/local/share/fonts:/usr/share/fonts${TYPST_FONT_PATHS:+:}${TYPST_FONT_PATHS}
COPY --link --from=newcm /usr/local/share/fonts /usr/local/share/fonts
COPY --link --from=inria /usr/local/share/fonts /usr/local/share/fonts
COPY --link --from=firago /usr/local/share/fonts /usr/local/share/fonts
COPY --link --from=firamath /usr/local/share/fonts /usr/local/share/fonts
COPY --link --from=ttf-ms-win11 /usr/local/share/fonts /usr/local/share/fonts
