# ACS Typst Docker Image

This Docker image ships [Typst] as well the following fonts:

- [New Computer Modern](#new-computer-modern)
- [Inria Fonts](#inria-fonts)
- [FiraGO](#firago)
- [Core fonts for the Web](#core-fonts-for-the-web)

[Typst]: https://github.com/typst/typst

## New Computer Modern

This image ships all variants of the [New Computer Modern] font, a drop-in replacement for the default LaTeX font [Computer Modern].

The [New Computer Modern] font was derived from [Computer Modern] as follows:

```mermaid
flowchart TD
  cm["Computer Modern\n(created by Donald Knuth)"]-->|manual recreation by Blue Sky Research|amsfonts
  amsfonts["AMSFonts \n(with font hinting)"]-->|conversion to OpenType|lm
  lm["Latin Modern\n(standard in the TeX community for Unicode)"]-->|extensions|newcm
  newcm["New Computer Modern\n(standard TeX font in Typst)"]
  click cm "https://ctan.org/pkg/cm"
  click amsfonts "https://ctan.org/pkg/amsfonts"
  click lm "https://ctan.org/pkg/lm"
  click newcm "https://ctan.org/pkg/newcomputermodern"
```

For more details, see [Comparison of vector fonts of “Computer Modern” variants—TeX—LaTeX Stack Exchange] and [Computer Modern—Wikipedia].

[New Computer Modern]: https://ctan.org/pkg/newcomputermodern
[Computer Modern]: https://ctan.org/pkg/cm
[Comparison of vector fonts of “Computer Modern” variants—TeX—LaTeX Stack Exchange]: https://tex.stackexchange.com/a/656644/291105
[Computer Modern—Wikipedia]: https://en.wikipedia.org/wiki/Computer_Modern#New_Computer_Modern

### Font Families

Font families can be set via [the text function's font property](https://typst.app/docs/reference/text/text/#parameters-font).

- `New Computer Modern` is also included in the official Typst binary (optimized for a font size of 10 point).
  - `NewComputerModern08` is optimized for a font size of 8 point.
- `New Computer Modern Math` is a math variant of the font.
  This variant is also included with the Typst binary and used by default for math.
- `New Computer Modern Mono` is a [monospaced] variant of the font.
- `New Computer Modern Sans` is a [sans-serif] variant of the font (optimized for a font size of 10 point).
  - `NewComputerModernSans08` is optimized for a font size of 8 point.
- `New Computer Modern Uncial` is an [uncial] (medieval Latin and Greek) variant of the font (optimized for a font size of 10 point).
  - `NewCMUncial08` is optimized for a font size of 8 point.
- `NewCM10Devanagari` is a [devanagari] (northern Indian) variant of the font.
  - `NewCM08Devanagari` is optimized for a font size of 8 point.

[monospaced]: https://en.wikipedia.org/wiki/Monospaced_font
[sans-serif]: https://en.wikipedia.org/wiki/Sans-serif
[uncial]: https://en.wikipedia.org/wiki/Uncial_script 
[devanagari]: https://en.wikipedia.org/wiki/Devanagari

### Font Style

The font style can be set via [the text function's style property](https://typst.app/docs/reference/text/text/#parameters-style).

- `normal` corresponds to the normal variants of the fonts.
- `italic` corresponds to the `-Italic` variants of the fonts and falls back to the `-Oblique` variants.
- `oblique` corresponds to the `-Oblique` variants of the fonts and falls back to the `-Italic` variants.

### Font Weights

The font weight can be set via [the text function's weight property](https://typst.app/docs/reference/text/text/#parameters-weight).

- `regular` and less weight corresponds to the `-Regular` variants of the fonts.
- `medium` corresponds to the `-Book` variants of the fonts.
- `semibold` and more weight corresponds to the `-Bold` variants of the fonts.

### Font Sizes

The font size can be set via [the text function's size property](https://typst.app/docs/reference/text/text/#parameters-size).
This does not affect the font selection, though.
You can optimize font selection manually by switching the [font family](#font-families), though.

## Inria Fonts

This image ships the [Inria Fonts] (`Inria Sans` and `Inria Serif`).

The [Inria Fonts] are used for the [Simple] and [Clean] themes of [Polylux] for creating slides.

[Inria Fonts]: https://black-foundry.com/work/inria/
[Simple]: https://polylux.dev/book/themes/gallery/simple.html
[Clean]: https://polylux.dev/book/themes/gallery/clean.html

## FiraGO and FiraMath

This image ships the [FiraGO] (`FiraGO`) and [FiraMath] (`Fira Math`) fonts.

The [FiraGO] and [FiraMath] fonts are used for the [Metropolis] theme of [Polylux] for creating slides.

[FiraGO]: https://bboxtype.com/typefaces/FiraGO
[FiraMath]: https://firamath.github.io/
[Metropolis]: https://polylux.dev/book/themes/gallery/metropolis.html
[Polylux]: https://github.com/andreasKroepelin/polylux

## Core fonts for the Web

This image ships the [core fonts for the Web] such as `Arial`, `Comic Sans MS`, `Courier New`, and `Times New Roman`.

These fonts are required for some RWTH templates.

[core fonts for the Web]: https://en.wikipedia.org/wiki/Core_fonts_for_the_Web

## Usage

You can now run Typst as follows:

```bash
docker run -it --init --pull always -v .:/root registry.git.rwth-aachen.de/acs/public/software/typst-docker:latest typst <COMMAND>
```

### Compile Once

This command compiles the `<INPUT>` file into PDF:

```bash
docker run -it --init --pull always -v .:/root registry.git.rwth-aachen.de/acs/public/software/typst-docker:latest typst compile <INPUT>
```

### Watch for Changes

This command watches for changes and recompiles the PDF automatically:

```bash
docker run -it --init --pull always -v .:/root registry.git.rwth-aachen.de/acs/public/software/typst-docker:latest typst watch <INPUT>
```

## Manual Setup

If you wish to recreate the same setup without using Docker, follow these steps:

1. [Install Typst](https://github.com/typst/typst#installation).

2. Install the New Computer Modern fonts.

   1. Download the latest release from https://download.gnu.org.ua/release/newcm/.

      ```bash
      curl -LO "https://download.gnu.org.ua/release/newcm/newcm-5.1.txz"
      ```

   2. Extract the archive.

      ```bash
      tar xvf newcm-5.1.txz
      ```

   3. Move the fonts into a directory of your choice.

      ```bash
      mv newcm-5.1/otf $HOME/.local/share/fonts/otf/NewCM
      ```

   4. Make the fonts discoverable by Typst by setting the `TYPST_FONT_PATHS` environment variable.

      ```bash
      export TYPST_FONT_PATHS=$HOME/.local/share/fonts
      ```

      To make this change permanent, follow [the steps for your shell](https://unix.stackexchange.com/a/117470/599161).

   5. Verify that Typst discovers the fonts.

      ```bash
      $ typst fonts
      New Computer Modern
      New Computer Modern Math
      New Computer Modern Mono
      New Computer Modern Sans
      New Computer Modern Uncial
      NewCM08Devanagari
      NewCM10Devanagari
      NewCMUncial08
      NewComputerModern08
      NewComputerModernSans08
      ```

3. Install the Inria Fonts by repeating the previous step, downloading the fonts from https://github.com/BlackFoundryCom/InriaFonts/releases.

4. Install the FireGO and FiraMath fonts by repeating the same step, downloading the fonts from https://github.com/bBoxType/FiraGO and https://github.com/firamath/firamath/releases.

5. Install the core fonts for the Web.
   They are already bundled with Windows and macOS.
   See https://corefonts.sourceforge.net for installing these fonts on Linux.
